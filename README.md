## Soumi Live

*Bootstraped with create-react-app*

#### Commands
* `yarn` or `npm install` to install all the dependencies
* `yarn start` or `npm start` : Start the app with HMR
* `yarn build` or `npm run build` Production build


#### Dependencies

* `antd` Ant design language
* `react-router-dom` React router v4 for brower
* `redux` State management lib
* `react-redux` React bindings for Redux.
* `redux-thunk` Thunk middleware for Redux.

#### Dev Dependencies

* `babel-plugin-import` Modular import plugin for babel, compatible with antd.
* `react-app-rewired` Tweak the create-react-app webpack config(s) without using 'eject' and without creating a fork of the react-scripts.
* `redux-immutable-state-invariant` Redux middleware that detects mutations between and outside redux dispatches.
* `redux-logger` Logs all the redux actions dispached.

