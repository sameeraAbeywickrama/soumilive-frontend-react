import { SCREENS__DASHBOARD_ROUTE_PERFORMED } from '../action-types';

/**
 * Creates action for dashboard screen routes.
 * @param {String} prev previous screen
 * @param {String} current screen to be set
 */
export const setDashboardScreens = (prev, current) => ({
    type: SCREENS__DASHBOARD_ROUTE_PERFORMED,
    payload: {prev, current}
})