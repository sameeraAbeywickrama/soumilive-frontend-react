import {
    AUTH__PERFORMING_USER_LOGIN_STARTED,
    AUTH__PERFORMING_USER_LOGIN_SUCCESSS,
    AUTH__PERFORMING_USER_LOGIN_FAILED
} from '../action-types';

import { login } from '../../API/auth.api' ;

/**
 * Creates actions for user login.
 */
const performLogin = () => {

    return dispatch => {

        dispatch ({
            type: AUTH__PERFORMING_USER_LOGIN_STARTED
        });

        login().then(data => {
            // if resolved success
            dispatch({
                type: AUTH__PERFORMING_USER_LOGIN_STARTED,
                playload: data
            });
        }).then (error => {
            // if resolved failed
            dispatch({
                type: AUTH__PERFORMING_USER_LOGIN_FAILED,
                payload: error
            });
        });

    }

}