import { combineReducers } from 'redux';
import { screenReducer } from './screens.reducer';

const reducers = combineReducers({
    screens: screenReducer
})

export default reducers;