import { SCREENS__DASHBOARD_ROUTE_PERFORMED } from '../action-types';

export const screenReducer = (state={current: 'Home', prev: ''}, action) => {
    switch(action.type) {
        case SCREENS__DASHBOARD_ROUTE_PERFORMED:
            return {current: action.payload.current, prev: action.payload.prev}

        default:
            return state;
    }
}