import React, { Component } from 'react';

import { connect } from 'react-redux';
import { Form, Icon, Input, Button } from 'antd';
import { loginWrapperStyles, loginFormStyles, formItemStyles } from './login.styles';
import { Link } from 'react-router-dom';

const FormItem = Form.Item;

/**
 * Class representing Login comonent
 */
class Login extends Component {

    /**
     * Handle form submits
     * @param {SyntheticEvent} event Event triggered with form submit
     */
    handleSubmit = event => {

        event.preventDefault();

        // If no errors then perform login
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });

    }

    render() {

        const { getFieldDecorator } = this.props.form;

        return (
            <div style={loginWrapperStyles}>
                <Form onSubmit={this.handleSubmit} style={loginFormStyles}>
                
                    {/* Logo */}
                    <div style={{fontWeight: 'bold', fontSize: 22}}><span style={{color: '#9c9c9c'}}>Soumi</span><span style={{color: '#1890ff'}}>Live</span></div>

                    {/* Username */}
                    <FormItem style={formItemStyles}>
                        {getFieldDecorator('userName', {
                            rules: [{ required: true, message: 'Please enter your username' }],
                        })(
                            <Input type="email" prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                        )}
                    </FormItem>

                    {/* Password */}
                    <FormItem style={formItemStyles}>
                        {getFieldDecorator('password', {
                            rules: [{ required: true, message: 'Please enter your Password!' }],
                        })(
                            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                        )}
                    </FormItem>

                    {/* Submit button */}                   
                    <FormItem>                        
                        <Button style={{width: '100%'}} type="primary" htmlType="submit" className="login-form-button">Log in</Button>
                    </FormItem>

                    {/* Login links */}                    
                    <div className="login-links">
                        <Link to="/register">Register</Link><Link to="/reset-password" href="">Reset password</Link>
                    </div>

                </Form>
            </div>
        )

    }
}

/**
 * Map app state to component props
 */
const mapStateToProps = () => {
    return {}
}

/**
 * Map redux dispatch to component props
 */
const mapDispatchToProps = dispatch => {
    return {}
}

// Decorating component with rcform.
Login = Form.create()(Login)

export default connect(mapStateToProps, mapDispatchToProps)(Login);