// Login component wrapper styles
export const loginWrapperStyles = {
    display: 'flex',
    flexWrap: 'wrap',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
};

// Login component form styles
export const loginFormStyles = {
    background: '#fff',
    padding: 10,
    width: 300,
    textAlign: 'center',
    paddingTop: 20
}

// Login component form item styles
export const formItemStyles = {
    textAlign: 'left'
}