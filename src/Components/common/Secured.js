import React from 'react';

import { Route, Redirect } from 'react-router-dom';

/**
 * Dumb component representing Secured routes.
 */

// eslint-disable-next-line
const Secured = ({ Component: Component, loggedIn: loggedIn, ...rest }) => {
    return (
        <Route
            {...rest}
            render={(props) => (
                loggedIn===true?<Component {...props} />:<Redirect to="/login" />
            )}
        />
    )
}


export default Secured;
