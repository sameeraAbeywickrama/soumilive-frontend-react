import React from 'react';

import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

/**
 * Dumb component representing PageNav
 */
const PageNav = ({prev, current}) => <div style={{display: 'none'}}>You on <Link to={`/dashboard/${current}`}>{current}</Link> {prev?(<span>Go back to <Link to={`/dashboard/${prev}`}>{prev}</Link></span>):null}</div>;

/**
 * Map redux state to component props.
 */
const mapStateToProps = ({screens}) => {

    const { prev, current } = screens;
    
    return {
        prev,
        current
    }

}

export default connect(mapStateToProps, null)(PageNav);

