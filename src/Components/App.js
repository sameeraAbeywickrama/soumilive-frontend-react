import React, { Component } from 'react';
import './App.css';

// 3rd party
import { Layout } from 'antd';
import { BrowserRouter as Router, Route } from 'react-router-dom';

// Custom components
import Login from './Login/Login';
import Dashboard from './Dashboard/Dashboard';
import Secured from './common/Secured';
import Register from './Register/Register';
import ResetPassword from './ResetPassword/ResetPassword';

// Styles
import { appLayoutWrapperStyles } from './app.styles';

/**
 * Class representing App component.
 */
class App extends Component {

	state = {
		loggedIn: true
	}

	render() {

		const { loggedIn } = this.state;

		return (

			// App wrapper
			<div className="App">
				
				{/* Routing */}
				<Router>

					<Layout style={appLayoutWrapperStyles}>
						
						{/* Dashboard */}
						<Secured
							path="/dashboard"
							Component={Dashboard}
							loggedIn={loggedIn}
							
						/>

						{/* Dashboard */}
						<Secured
							exact
							path="/"
							Component={Dashboard}
							loggedIn={loggedIn}
							
						/>

						{/* Login */}
						<Route
							exact
							path="/login"
							render={() => <Login />}
						/>

						{/* Register */}
						<Route
							exact
							path="/register"
							render={() => <Register />}
						/>

						{/* Reset password */}
						<Route
							exact
							path="/reset-password"
							render={() => <ResetPassword />}
						/>
						
					</Layout>
				</Router>

				
			</div>
		);
	}
}

export default App;
