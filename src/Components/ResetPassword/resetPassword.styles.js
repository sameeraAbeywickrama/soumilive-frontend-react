// reset password component wrapper styles
export const resetPasswordWrapperStyles = {
    display: 'flex',
    flexWrap: 'wrap',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
};

// reset password component form styles
export const resetPasswordFormStyles = {
    background: '#fff',
    padding: 10,
    width: 300,
    textAlign: 'center',
    paddingTop: 20
}

// reset password component form item styles
export const formItemStyles = {
    textAlign: 'left',
    marginTop: 20
}