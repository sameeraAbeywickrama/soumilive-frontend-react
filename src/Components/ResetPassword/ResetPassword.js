import React, { Component } from 'react';

import { connect } from 'react-redux';
import { Form, Icon, Input, Button } from 'antd';
import { formItemStyles, resetPasswordFormStyles, resetPasswordWrapperStyles}  from './resetPassword.styles'
import { Link } from 'react-router-dom';

const FormItem = Form.Item;

/**
 * Class representing ResetPassword component.
 */
class ResetPassword extends Component {

    render () {

        const { getFieldDecorator } = this.props.form;

        return (
            <div style={resetPasswordWrapperStyles}>
                <Form onSubmit={this.handleSubmit} style={resetPasswordFormStyles}>
                
                    {/* Logo */}
                    <div style={{fontWeight: 'bold', fontSize: 22}}><span style={{color: '#9c9c9c'}}>Soumi</span><span style={{color: '#1890ff'}}>Live</span></div>

                    {/* Password */}
                    <FormItem style={formItemStyles}>
                        {getFieldDecorator('email', {
                            rules: [{ required: true, message: 'Please enter your email!' }],
                        })(
                            <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} type="email" placeholder="johndoe@email.com" />
                        )}
                    </FormItem>

                    {/* Submit button */}                   
                    <FormItem>                        
                        <Button style={{width: '100%'}} type="primary" htmlType="submit" className="login-form-button">Send Email</Button>
                    </FormItem>

                    {/* Login links */}                    
                    <div className="login-links">
                        <Link to="/login">Login</Link><Link to="/register" href="">Register</Link>
                    </div>

                </Form>
            </div>
        )
    }
}

/**
 * Map app state to component props
 */
const mapStateToProps = () => {
    return {}
};

/**
 * Map redux dispatch to component props
 */
const mapDispatchToProps = dispatch => {
    return {}
};

// Decorating component with rcform.
ResetPassword = Form.create()(ResetPassword)

export default connect(mapStateToProps, mapDispatchToProps) (ResetPassword);