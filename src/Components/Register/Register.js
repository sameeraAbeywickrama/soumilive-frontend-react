import React, { Component } from 'react';

import { connect } from 'react-redux';
import { Form, Icon, Input, Button, Col, Row } from 'antd';
import { registerWrapperStyles, registerFormStyles, formItemStyles } from './register.styles';

import { Link } from 'react-router-dom';

const FormItem = Form.Item;

/**
 * Class representing Register component.
 */
class Register extends Component {

    state = {
        isConfirmDirty: false,
    }

    /**
     * Handle form submits
     * @param {SyntheticEvent} event Event triggered with form submit
     */
    handleSubmit = event => {

        event.preventDefault();

        // If no errors then perform user registration
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });

    }

    /**
     * Compare and validate password field with confirmPassword
     * @param {Object} rule Describes rule defined.
     * @param {String} value Value to be validated
     * @param {Function} callback callback to be executed with validations.
     */ 
    validateWithPasswordConfirm = (rule, value, callback) => {

        const form = this.props.form;

        if (value && this.state.isConfirmDirty) {
            form.validateFields(['confirmPassword'], { force: true });
        }
        
        callback();
    }

    /**
     * Compare and validate confirmPassword field with password field
     * @param {Object} rule Describes rule defined.
     * @param {String} value Value to be validated
     * @param {Function} callback callback to be executed with validations.
     */ 
    validateWithPassword = (rule, value, callback) => {

        const form = this.props.form;

        if (value && value !== form.getFieldValue('password')) {
            callback('Two passwords that you enter is inconsistent!');
        } else {
            callback();
        }

    }

    /**
     * Handles blur event triggered by confirm password.
     * @param {SyntheticEvent} event Event triggered by form field blur.
     */
    handlePasswordConfirmBlur = event=> {
        const value = event.target.value;
        this.setState({ isConfirmDirty: this.state.isConfirmDirty || !!value });
    }

    render() {

        const { getFieldDecorator } = this.props.form;

        return (
            <div style={registerWrapperStyles} className="registration-wrapper">
                <Form onSubmit={this.handleSubmit} style={registerFormStyles} className="registration-form">

                    <Row gutter={16}>

                        <Col md={24}>
                            {/* Logo */}
                            <div style={{ fontWeight: 'bold', fontSize: 22 }}><span style={{ color: '#9c9c9c' }}>Soumi</span><span style={{ color: '#1890ff' }}>Live</span></div>
                        </Col>

                        <Col md={12}>
                            {/* First Name */}
                            <FormItem label="First Name" style={formItemStyles}>
                                {getFieldDecorator('firstName', {
                                    rules: [{ required: true, message: 'Please enter your first name' }],
                                })(
                                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="John" />
                                )}
                            </FormItem>
                        </Col>

                        <Col md={12}>
                            {/* Last Name */}
                            <FormItem label="Last Name" style={formItemStyles}>
                                {getFieldDecorator('lastName', {
                                    rules: [{ required: true, message: 'Please enter your last name' }],
                                })(
                                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Doe" />
                                )}
                            </FormItem>
                        </Col>

                    </Row>
                    <Row gutter={16}>

                        <Col md={12}>
                            {/* Password */}
                            <FormItem label="Password" style={formItemStyles}>
                                {getFieldDecorator('password', {
                                    rules: [{
                                        required: true, message: 'Please enter the password'
                                    }, {
                                        validator: this.validateWithPasswordConfirm,
                                    }],
                                })(
                                    <Input type="password" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="*******" />
                                )}
                            </FormItem>
                        </Col>

                        <Col md={12}>
                            {/* Repeat Password */}
                            <FormItem label="Confirm Password" style={formItemStyles}>
                                {getFieldDecorator('confirmPassword', {
                                    rules: [{
                                        required: true, message: 'Please Confirm your password'
                                    }, {
                                        validator: this.validateWithPassword,
                                    }],
                                })(
                                    <Input onBlur={this.handlePasswordConfirmBlur}  type="password" prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="*******"/>
                                )}
                            </FormItem>
                        </Col>

                    </Row>
                    <Row gutter={16}>

                        <Col md={12}>
                            {/* Email */}
                            <FormItem label="Email" style={formItemStyles}>
                                {getFieldDecorator('email', {
                                    rules: [{ required: true, message: 'Please enter your email' }],
                                })(
                                    <Input type="email" prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="johndoe@email.com" />
                                )}
                            </FormItem>
                        </Col>

                        <Col md={12}>
                            {/* Mobile */}
                            <FormItem label="Mobile" style={formItemStyles}>
                                {getFieldDecorator('mobile', {
                                    rules: [{ required: true, message: 'Please enter your mobile numbder' }],
                                })(
                                    <Input type="tel" prefix={<Icon type="mobile" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="+94771231234" />
                                )}
                            </FormItem>
                        </Col>

                    </Row>
                    <Row>
                        <Col md={24}>
                            {/* Submit button */}
                            <FormItem>
                                <Button style={{ width: '100%' }} type="primary" htmlType="submit" className="login-form-button">Register</Button>
                            </FormItem>

                            {/* Login links */}
                            <div className="login-links">
                                <Link to="/Login">I already have an account</Link><Link to="/reset-password" href="">Reset password</Link>
                            </div>
                        </Col>
                    </Row>
                </Form>
            </div>
        )
    }
}


/**
 * Map redux state to component props.
 */
const mapStateToProps = () => {
    return {}
}
/**
 * Map redux dispatch to componet props.
 */
const mapDispatchToProps = dispatch => {
    return {}
}

// Decorating component with rcform.
Register = Form.create()(Register);

export default connect(mapStateToProps, mapDispatchToProps)(Register);