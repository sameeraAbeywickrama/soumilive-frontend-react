// Register component wrapper styles
export const registerWrapperStyles = {
    display: 'flex',
    flexWrap: 'wrap',
    height: '100%',
    alignItems: 'top',
    justifyContent: 'center',
};

// Register component Form styles
export const registerFormStyles = {
    background: '#fff',
    padding: 16,
    textAlign: 'center',
    paddingTop: 20,
    width: '100%'
}

// Register component Form Item styles
export const formItemStyles = {
    textAlign: 'left'
}