import React, { Component } from 'react';
import { Layout, Col, Row, Card } from 'antd';
import { cardWrapperStyles } from './quiz.styles';

import { connect } from 'react-redux';

const { Content } = Layout;

class Quiz extends Component {

    render () {

        return <Content style={{margin: '66px 16px 16px'}}>
            <Row gutter={16}>
                <Col md={12}>
                    <Card style={cardWrapperStyles} title="Quizzes" bordered={false}></Card>
                </Col>
                <Col md={12}>
                    <Card style={cardWrapperStyles} title="Assignments" bordered={false}></Card>
                </Col>
            </Row>
        </Content>
    }
}

/**
 * Map redux state to component props.
 */
const mapStateToProps = () => {
    return {}
}
/**
 * Map redux dispatch to componet props.
 */
const mapDispatchToProps = dispatch => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps) (Quiz);