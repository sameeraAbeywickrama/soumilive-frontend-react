import React, { Component } from 'react';

import { Layout } from 'antd';

const { Content } = Layout;

class Home extends Component {

    render () {
        return <Content style={{margin: '66px 16px 16px', background: '#fff'}}>
            
        </Content>
    }
}

export default Home;