import React, { Component } from 'react';

// 3rd arty
import { Layout, Menu, Icon } from 'antd';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { setDashboardScreens } from '../../../redux/actions/screens.dashboard.ac';

const { Sider } = Layout;

// Menu items array
const menuItems = [{
    icon: 'home',
    text: 'Home',
    path: 'home'
}, {
    icon: 'edit',
    text: 'Quiz',
    path: 'quiz'
}];

/**
 * Class representing Dashboard Aside
 */
class DashboardAside extends Component {

    /**
     * Fired with menu clicks
     * @param {Object} item Object describing clicked menu item
     */
    onMenuClick = item => {
        this.props.setScreen(this.props.prevScreen, menuItems[item.key].path)
    }

    render () {

        /**
         * Iterate through items array and renders items
         * @returns rendered menu items
         */
        const renderMenuItems = () => menuItems.map((item, index) => <Menu.Item key={index}>
            <Link to={`/dashboard/${item.path}`}>
                <Icon type={item.icon} />
                <span>{item.text}</span>
            </Link>
        </Menu.Item>)


        return (
            <Sider
                breakpoint="lg"
                collapsedWidth="0"
                onCollapse={(collapsed, type) => { this.props.handleAsideCollapse(collapsed, type); }}
            >
                {/* Logo */}
                <div style={{paddingTop: 10, paddingBottom: 22, textAlign: 'center', fontWeight: 'bold', fontSize: 22}}><span style={{color: '#ffffff'}}>Soumi</span><span style={{color: '#1890ff'}}>Live</span></div>

                {/* Aside menu */}
                <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']} onClick={this.onMenuClick}>
                    {renderMenuItems()}
                </Menu>

            </Sider>
        )
    }
}


/**
 * Map redux state to component props.
 */
const mapStateToProps = ({screens}) => {
    return {
        prevScreen: screens.current
    }
}
/**
 * Map redux dispatch to componet props.
 */
const mapDispatchToProps = dispatch => {
    return {
        setScreen: (prev, current)=>dispatch(setDashboardScreens(prev, current))
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (DashboardAside);