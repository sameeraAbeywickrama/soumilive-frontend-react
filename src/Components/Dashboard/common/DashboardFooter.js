import React from 'react';

import { Layout } from 'antd';

const { Footer } = Layout;

/**
 * Dumb component representing DashboardFooter.
 */
const DashboardFooter = () => {
    return (
        <Footer style={{ textAlign: 'center' }}>
            Footer here
        </Footer>
    )
}

export default DashboardFooter;