import React, { Component } from 'react';

import {
	Navbar,
	Nav,
	NavItem
} from 'react-bootstrap';

const { Header, Brand, Toggle, Collapse } = Navbar;

/**
 * Class representing Navigation bar.
 */
class Navigation extends Component {

	state = {
		isOpen: false
	}

	/**
	 * Open and close the navigation panel on mobile.
	 */
	toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}

	render() {

		const { collapsed } = this.props;

		return (
			<Navbar fluid collapseOnSelect fixedTop style={{ zIndex: 99, background: '#fff', width: !collapsed ? 'calc(100% - 200px)' : '100vw', right: 0, left: 'auto', transition: 'width 0.175s' }}>

				{/* Nav header containing brand */}
				<Header>
					{collapsed ? <Brand><div style={{ paddingLeft: 46, paddingTop: 10, paddingBottom: 22, textAlign: 'center', fontWeight: 'bold', fontSize: 22 }}><span style={{ color: '#ababab' }}>Soumi</span><span style={{ color: '#1890ff' }}>Live</span></div></Brand> : null}
					<Toggle />
				</Header>

				{/* Collapsible content */}
				<Collapse>
					<Nav pullRight>
						<NavItem eventKey={1} href="#">
							Link Right
            			</NavItem>
						<NavItem eventKey={2} href="#">
							Link Right
            			</NavItem>
					</Nav>
				</Collapse>
			</Navbar>
		)
	}
}


export default Navigation;
