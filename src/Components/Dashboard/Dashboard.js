import React, { Component } from 'react';

import { Layout } from 'antd';
import { Route } from 'react-router-dom';

// Common components
import DashboardAside from './common/DashboardAside';
import DashboardFooter from './common/DashboardFooter';
import Navigation from './common/Navigation';


import PageNav from '../common/PageNav';

// Sections
import Home from './subComponents/Home';
import Quiz from './subComponents/Quiz';


/**
 * Class representing Dashboard component.
 */
class Dashboard extends Component {

    state = {
        collapsed: false
    }

    /**
     * Triggeres with aside allapse state changes and recode it in component state.
     * @param {Boolean} collapsed indicates whether the aside is open or closed.
     * @param {String} type  collapse type
     */
    handleAsideCollapse = (collapsed, type) => {
        this.setState({collapsed});
    }

    render() {

        const { collapsed } = this.state;
        const { match } = this.props;

        return (
            <Layout>

                {/* Dashboard aside */}
                <DashboardAside
                    handleAsideCollapse={this.handleAsideCollapse}
                />

                {/* Dashboard navigation */}
                <Navigation
                    collapsed={collapsed}
                />

                {/* Main Layout */}
                <Layout>
                    
                    {/* Sub routes:Home */}
                    <Route 
                        path={`${match.url}/home`}
                        component={Home}
                    />

                    {/* Sub routes:quizz */}
                    <Route 
                        path={`${match.url}/quiz`}
                        component={Quiz}
                    />

                    {/* Mainfoter */}
                    <DashboardFooter />

                    <PageNav />

                </Layout>
            </Layout>
        )
    }
}

export default Dashboard;
